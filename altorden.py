def pascal(n):
    if n==1:
        return [1]
    else:
        r=pascal(n-1)
        x=[0]+r
        y=r+[0]
        return [i+j for i,j in zip(x,y)]

def conc(func,n):
    nueva = [0]+func(n)
    print(nueva)

def concs(func,n):
    nueva = func(n)+func(n+1)
    print(nueva)
    
conc(pascal,5)
concs(pascal,5)
