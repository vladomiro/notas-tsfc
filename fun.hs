pari :: Integral a => a -> Bool
pari n = n `mod` 2 == 0

divi :: Int -> [a] -> ([a],[a])
divi n xs = (take n xs, drop n xs)

reste :: [Float] -> [Float] -> [Float]
reste xs ys = zipWith (-) xs ys

absol :: Int -> Int
absol n = if n >= 0 then n else -n

signi :: Int -> String
signi n = if n < 0 then "negativo" else
            if n == 0 then "cero" else "positivo"

signg :: Int -> String
signg n | n < 0 = "negativo"
        | n > 0 = "positivo"
        |otherwise = "cero"

cabeza :: (a,b) -> a
cabeza (x,_) = x

prob :: [Char] -> Bool
prob ['a',_,_] = True
prob _         = False

cabezo :: [a] -> a
cabezo (x:_) =x

suma :: Int -> Int -> Int
suma = \x -> (\y -> x+y)

consta :: a -> (b -> a)
consta x = \_ -> x

impa :: Int -> [Int]
impa n = map f [0..n-1]
         where f x = x*2 + 1

impa2 :: Int -> [Int]
impa2 n = map (\x -> x*2 + 1) [0..n-1]

tam :: [a] -> Int
tam xs = sum [1| _ <- xs]

factores :: Int -> [Int]
factores n = [x | x <- [1..n], n `mod` x == 0]

primo :: Int -> Bool
primo n = factores n == [1,n]

primos :: Int -> [Int]
primos n = [x | x <- [2..n], primo x]

busca :: Eq a => a -> [(a,b)] -> [b]
busca k t = [v | (k',v) <- t, k==k']
