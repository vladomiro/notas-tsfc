def nats(n):
    yield n
    yield from nats(n+1)
    
s=nats(1)

