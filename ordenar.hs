ordenar [] = []
ordenar (x:xs) = ordenar chico ++ [x] ++ ordenar grande
  where
    chico =[a | a<-xs, a<=x]
    grande = [b | b<- xs, b>x]
