def pascal(n):
    if n==1:
        return [1]
    else:
        r=pascal(n-1)
        x=[0]+r
        y=r+[0]
        return [i+j for i,j in zip(x,y)]

def nats(n):
    yield n
    yield from nats(n+1)
    
s=nats(1)
print(pascal(next(s)))

