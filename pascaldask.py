import dask

@dask.delayed

def pascalazy(n):
    if n==1:
        return [1]
    else:
        r=pascal(n-1)
        x=[0]+r
        y=r+[0]
        return [i+j for i,j in zip(x,y)]

    
pascalazy(5).compute()
