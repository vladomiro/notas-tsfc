import Data.Set as Set
doRep x n | n <= 0 = []
          | otherwise = x:doRep x (n-1)

miLista = [1,4,8,2,3,3,5,1,6]
miConj = Set.fromList(miLista)

