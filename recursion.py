def doRep(x, n):
    y = []
    if n==0:
        return []
    else:
        y = doRep(x,n-1)
        print(y)
        y.append(x)
        return y

def lSum(list):
    if not list:
        return 0
    else:
        return list[0]+lSum(list[1:])

lSum2 = lambda list: 0 if not list \
    else list[0] + lSum2(list[1:])

miLista = [1,4,8,2,3,3,5,1,6]
miConj = set(miLista)
